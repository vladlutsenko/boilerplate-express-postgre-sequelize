const db = require('../../db/models');
import User from './interface';

export async function registerUser(user: User) {
    const newuser = await db.User.create({
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      password: user.password
    });
    return newuser;
}

export async function findUserByEmail(email: string) {
    const user = await db.User.findOne({
        where: {
          email: email
        }
    })
    return user;
}

export async function getAllUsers() {
    const users = await db.User.findAll();
    return users;
}