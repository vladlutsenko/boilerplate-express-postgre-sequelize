import express from 'express';
import * as service from './service';
import HttpException from '../exceptions/HttpException';


export async function registerUser(req: express.Request, res: express.Response, next: express.NextFunction) {
  try {
    const newuser = await service.registerUser(req.body);
    res.json({
      newuser
    });
  } catch (error) {
      next(new HttpException(400, 'Registartion error'));
  }
}

export async function loginUser(req: express.Request, res: express.Response, next: express.NextFunction) {
  try {
    const loggedToken = await service.loginUser(req.body.email, req.body.password);
    res.json({
      loggedToken
    }); 
  } catch (error) {
    next(error);
  }
}

export async function getAllUsers(req: express.Request, res: express.Response) {
  const users = await service.getAllUsers();
  res.json({
    users
  });
}

export function someBasicFuncJustForFun(number:number) {
  return ++number;
}
