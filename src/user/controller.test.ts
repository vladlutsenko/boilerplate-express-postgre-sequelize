const {someBasicFuncJustForFun} = require('./controller')

describe('Test 1', function () {

  it('should not fail', function () {
    const result = someBasicFuncJustForFun(2);

    expect(result).toEqual(3);
  });


  afterAll(() => {
    
  });

});
