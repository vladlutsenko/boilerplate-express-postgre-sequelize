import * as model from './model';
import User from './interface';
import HttpException from '../exceptions/HttpException';
import bcrypt from 'bcrypt';
const jsonwebtoken = require('jsonwebtoken');
const { secret } = require('../../config/server');

export async function registerUser(user: User) {
    const newuser = await model.registerUser(user);
    return newuser;
}

export async function loginUser(email: string, password: string) {
    const user = await model.findUserByEmail(email);
  
    if (user) {
        if (await bcrypt.compare(password, user.dataValues.password)) {
          return jsonwebtoken.sign({
            sub: user.dataValues.id,
            data: {
                ...user.dataValues
            },
            exp: Math.floor(Date.now()/1000) + (60 * 60)
          }, secret)
        }  
        else{
            throw new HttpException(403, 'Password incorrect');
        }
    }
    else{
        throw new HttpException(404, 'User not found');
    }
  
}

export async function getAllUsers() {
    const users = await model.getAllUsers();
    return users;
}