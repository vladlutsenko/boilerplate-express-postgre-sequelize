import express from 'express';
import jwtVerifier from '../middlewares/jwtVerifier'
import * as controller from './controller'

const router = express.Router();


router.post('/login', controller.loginUser);

router.post('/register', controller.registerUser);

router.get('/', jwtVerifier, controller.getAllUsers);


export default router;