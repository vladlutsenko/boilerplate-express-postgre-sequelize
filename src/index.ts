import express from 'express';
import bodyParser from 'body-parser';
import errorHandler from './middlewares/errorHandler'
import cors from 'cors'

import userRouter from './user/router'


const app = express();
const port = process.env.SERVER_PORT;


/* Middlewares */
app.use(bodyParser.json());
app.use(cors());

app.use(express.static('static'));


/* Routers */
app.use('/user', userRouter);


/* Other endpoints */
app.get( "/healthcheck", ( req, res ) => {
    res.status(200).send('Server is up!');
});


/* Error handler */
app.use(errorHandler);


app.listen( port, () => {
    console.log( `server started at http://localhost:${ port }` );
} );