module.exports = {
    'development': {
        'username': process.env.POSTGRES_USER,
        'password': process.env.POSTGRES_PASSWORD,
        'database': process.env.POSTGRES_DB,
        'host': process.env.POSTGRES_HOST,
        'port': process.env.POSTGRES_PORT,
        'seederStorage': "sequelize",
        'dialect': 'postgres',
        "define": {
            "freezeTableName": "true",
        },
    },
    'test': {
      'username': process.env.POSTGRES_USER,
      'password': process.env.POSTGRES_PASSWORD,
      'database': process.env.POSTGRES_DB,
      'host': process.env.POSTGRES_HOST,
      'port': process.env.POSTGRES_PORT,
      'seederStorage': "sequelize",
      'dialect': 'postgres',
      "define": {
          "freezeTableName": "true",
      },
    },
    'stage': {
      'username': process.env.POSTGRES_USER,
      'password': process.env.POSTGRES_PASSWORD,
      'database': process.env.POSTGRES_DB,
      'host': process.env.POSTGRES_HOST,
      'seederStorage': "sequelize",
      'dialect': 'postgres',
      "define": {
        "freezeTableName": "true",
      },
    },
    'production': {
      'username': process.env.POSTGRES_USER,
      'password': process.env.POSTGRES_PASSWORD,
      'database': process.env.POSTGRES_DB,
      'host': process.env.POSTGRES_HOST,
      'seederStorage': "sequelize",
      'dialect': 'postgres',
      "define": {
        "freezeTableName": "true",
      },
    }
  };