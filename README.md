# Boilerplate express + postgre
## Before start:
1. Make sure .env filled correctly
2. Make sure folder pg_data exists

To start run following command:
```
docker-compose up
```
## Npm scripts:

Create new model:
```
npm run model:new
```

Create new migration:
```
npm run migration:new
```

Create new seeder:
```
npm run seeder:new
```

Run tests:
```
npm run test
```

Build js from ts:
```
npm run build
```

## Endpoints:

Healthcheck:
```
GET http://localhost:{SERVER_PORT}/healthcheck
```

Get users:
```
GET http://localhost:{SERVER_PORT}/user/
```

Login user:
```
POST http://localhost:{SERVER_PORT}/user/login
```
```json
{
    "email": "test@email.com",
    "password": "1234"
}
```

Register user:
```
POST http://localhost:{SERVER_PORT}/user/register
```
```json
{
    "email": "johndoe@email.com",
    "password": "4321",
    "firstName": "John",
    "lastname": "Doe"
}
```